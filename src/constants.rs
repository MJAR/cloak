use bevy::prelude::*;

pub struct HeroConstants {
    pub w: f32,
    pub h: f32,
}

pub struct MapConstants {
    pub w: f32,
    pub h: f32,
    pub margin: f32,
}

impl HeroConstants {
    pub fn vec(&self) -> Vec2 {
        Vec2 {
            x: self.w,
            y: self.h,
        }
    }
}

impl MapConstants {
    pub fn vec(&self) -> Vec2 {
        Vec2 {
            x: self.w,
            y: self.h,
        }
    }
}

pub const HERO: HeroConstants = HeroConstants { w: 50.0, h: 50.0 };
pub const MAP: MapConstants = MapConstants {
    w: 43.0,
    h: 25.,
    margin: 2.,
};
