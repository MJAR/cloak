use std::ops::Add;

use bevy::prelude::*;

use bevy::window::*;

pub mod constants;

pub mod hero;
pub mod map;
pub mod scenario;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(1.0, 1.0, 1.0)))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                resolution: WindowResolution::new(900., 400.),
                ..default()
            }),
            ..default()
        }))
        .add_systems(
            Startup,
            (
                hero::setup,
                map::setup,
                camera,
                scenario::setup,
                scenario::setup_achievements,
            ),
        )
        .add_systems(
            Update,
            (
                hero::update_movement_state,
                hero::update_position,
                hero::update_translation,
                hero::update_face_direction,
                hero::update_ordered_path,
                map::update,
                scenario::update_after_mouse_click,
                scenario::update_for_hero_move,
                scenario::book_update,
                scenario::update_text_position,
                scenario::update_text_according_to_scenario_stage,
                scenario::update_achievment_position_for_camera,
                scenario::update_achievement_graphics_according_to_visibility
            ),
        )
        .add_systems(Update, update_camera)
        .run();
}

fn camera(mut commands: Commands) {
    let mut camera = Camera2dBundle::default();

    camera.projection.scale = 0.6;

    commands.spawn(camera);
}

fn update_camera(
    mut query: Query<&mut Transform, With<Camera>>,
    query2: Query<&crate::hero::Hero>,
) {
    let hero: &crate::hero::Hero = query2.single();

    for mut transform in query.iter_mut() {
        transform.translation = hero.position.add(Vec2 {
            x: 0.,
            y: -20.
        }).extend(0.)
    }
}