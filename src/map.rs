use bevy::prelude::*;

#[derive(Debug, Eq, PartialEq, Clone, Component)]
pub struct Cell {
    pub x: i32,
    pub y: i32,
}

impl Cell {
    pub fn to_isometric(&self) -> Vec2 {
        let w = crate::constants::MAP.w + crate::constants::MAP.margin;
        let h = crate::constants::MAP.h + crate::constants::MAP.margin;

        let x_isometric: f32 = (w / 2.) * (self.x as f32 - self.y as f32);
        let y_isometric: f32 = -(h / 2.) * (self.x as f32 + self.y as f32);

        Vec2 {
            x: x_isometric,
            y: y_isometric,
        }
    }

    pub fn sub(&self, r: &Cell) -> Cell {
        Cell {
            x: self.x - r.x,
            y: self.y - r.y,
        }
    }

    pub fn from_isometric(x_isometric: f32, y_isometric: f32) -> Cell {
        let w = crate::constants::MAP.w + crate::constants::MAP.margin;
        let h = crate::constants::MAP.h + crate::constants::MAP.margin;

        let y_isometric = y_isometric - h / 2.0;
        let x = x_isometric / w - y_isometric / h;

        let y = -y_isometric / h - x_isometric / w;

        Cell {
            x: x.floor() as i32,
            y: y.floor() as i32,
        }
    }

    pub fn from_isometric_without_rounding(x_isometric: f32, y_isometric: f32) -> Vec2 {
        let w = crate::constants::MAP.w + crate::constants::MAP.margin;
        let h = crate::constants::MAP.h + crate::constants::MAP.margin;

        let y_isometric = y_isometric - h / 2.0;
        let x = x_isometric / w - y_isometric / h;

        let y = -y_isometric / h - x_isometric / w;

        Vec2 {x, y}
    }

    pub fn length(& self) -> f32 {
        ( (self.x * self.x + self.y + self.y) as f32 ).sqrt()
    }
}

pub fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    for x in -31..31 {
        for y in -31..31 {
            let cell = Cell { x, y };
            commands.spawn((picture(cell.to_isometric(), &asset_server), cell));
        }
    }
}

const C: f32 = 0.5;

const A: f32 =  2. ;//30.;
const B: f32 =  3.;

const P0: f32 = C / (A - B);
const D: f32 = - P0 * B;

fn calc_alpha(distance: f32) -> f32 {

    if distance < 0. {
        panic!("distance given to the calc_alpha have to be greater than 0");
    }

    if distance < A {
        C
    }
    else if distance < B {
        P0 * distance + D
    }
    else  {
        0.
    }
}

pub fn update(mut query: Query<( & mut Sprite, &Cell )>, query_hero: Query<&crate::hero::Hero>) {
    for (ref mut sprite, cell) in & mut query {

        let hero: &crate::hero::Hero = query_hero.single();

        //let distance = hero.position.distance(cell.to_isometric());

        let cell_hero_pos = Cell::from_isometric_without_rounding(hero.position.x, hero.position.y);

        let cell_float: Vec2 = Vec2 { x: cell.x as f32, y: cell.y as f32 };

        let distance = cell_hero_pos.distance(cell_float);

        sprite.color.set_a(calc_alpha(distance));
    }
}

fn picture(pos: Vec2, asset_server: &Res<AssetServer>) -> SpriteBundle {
    let mut sprite = Sprite {
        custom_size: Some(crate::constants::MAP.vec()),
        ..default()
    };

    sprite.color.set_a(0.4);

    SpriteBundle {
        texture: asset_server.load("cell2.png"),
        transform: Transform::from_translation(pos.extend(-2.)),
        sprite: sprite,
        ..default()
    }
}