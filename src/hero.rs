use std::ops::{Add, Sub};

use bevy::input::mouse::MouseButton;
use bevy::input::mouse::MouseButtonInput;
use bevy::input::ButtonState;
use bevy::prelude::*;

use crate::map::Cell;

#[derive(Debug, PartialEq)]
enum FaceDirection {
    RightFront,
    LeftFront,
    RightBack,
    LeftBack,

    Front,
    Back,
    Left,
    Right,
}

impl FaceDirection {
    fn index(&self) -> usize {
        match self {
            FaceDirection::RightFront => 2,
            FaceDirection::LeftFront => 0,
            FaceDirection::RightBack => 4,
            FaceDirection::LeftBack => 6,
            FaceDirection::Front => 1,
            FaceDirection::Back => 5,
            FaceDirection::Left => 7,
            FaceDirection::Right => 3,
        }
    }
}

#[derive(Debug)]
pub enum Movement {
    Move {
        starting_cell: Cell,
        target_cell: Cell,
    },
    Stand {
        cell: Cell,
    },
}

#[derive(Component, Debug)]
pub struct Hero {
    pub movement: Movement,
    direction: FaceDirection,
    pub path: Vec<Cell>,
    pub position: Vec2,
}

impl Movement {
    fn face_direction(&self) -> Option<FaceDirection> {
        match self {
            Movement::Move {
                starting_cell,
                target_cell,
            } => {
                let direction: Cell = target_cell.sub(starting_cell);

                if direction.x == 1 && direction.y == 0 {
                    Option::Some(FaceDirection::RightFront)
                } else if direction.x == -1 && direction.y == 0 {
                    Option::Some(FaceDirection::LeftBack)
                } else if direction.x == 0 && direction.y == 1 {
                    Option::Some(FaceDirection::LeftFront)
                } else if direction.x == 0 && direction.y == -1 {
                    Option::Some(FaceDirection::RightBack)
                } else if direction.x == 1 && direction.y == 1 {
                    Option::Some(FaceDirection::Front)
                } else if direction.x == -1 && direction.y == -1 {
                    Option::Some(FaceDirection::Back)
                } else if direction.x == 1 && direction.y == -1 {
                    Option::Some(FaceDirection::Right)
                } else if direction.x == -1 && direction.y == 1 {
                    Option::Some(FaceDirection::Left)
                } else {
                    panic!("difference between the starting cell and the target cell in the Movement state is not proper");
                }
            }
            _ => None,
        }
    }

    pub fn current_position(&self) -> Cell {
        match self {
            Movement::Move {
                starting_cell,
                target_cell: _,
            } => starting_cell.clone(),
            Movement::Stand { cell } => cell.clone(),
        }
    }
}

const UP_ALIGNMENT: Vec2 = Vec2 {
    x: 0.,
    y: crate::constants::HERO.h as f32 / 2. - 7.,
};

pub fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
) {
    let cell = Cell { x: 0, y: 0 };

    let start_position = cell.to_isometric().add(UP_ALIGNMENT);

    let path: Vec<Cell> = Vec::new();

    commands.spawn((
        Hero {
            movement: Movement::Stand { cell },
            direction: FaceDirection::RightFront,
            path,
            position: Vec2 { x: 0., y: 0. },
        },
        picture_sheet(start_position, &asset_server, &mut texture_atlases),
    ));
}

pub fn update_movement_state(mut query: Query<(&mut Hero, &mut Transform)>) {
    for mut bundle in &mut query {
        let hero: &mut Hero = bundle.0.as_mut();

        match &mut hero.movement {
            Movement::Stand { cell } => {
                if !hero.path.is_empty() {
                    hero.movement = Movement::Move {
                        starting_cell: Cell {
                            x: cell.x,
                            y: cell.y,
                        },
                        target_cell: hero.path.remove(0),
                    };
                }
            }
            Movement::Move {
                starting_cell: _,
                target_cell,
            } => {
                let target_position_isometric = target_cell.to_isometric();

                let current_position_isometric = hero.position; //transform.translation.truncate().sub(UP_ALIGNMENT);

                let distance = current_position_isometric.distance(target_position_isometric);

                if distance < 2.0 {
                    hero.movement = Movement::Stand {
                        cell: Cell {
                            x: target_cell.x,
                            y: target_cell.y,
                        },
                    };
                }
            }
        }
    }
}

const SPEED: f32 = 50.0;

pub fn update_position(time: Res<Time>, mut query: Query<&mut Hero>) {
    for mut bundle in &mut query {
        let hero: &mut Hero = bundle.as_mut();
        //let transform: &mut Transform = bundle.1.as_mut();

        match &mut hero.movement {
            Movement::Move {
                starting_cell,
                target_cell,
            } => {
                let target_isometric_position = target_cell.to_isometric();
                let start_isometric_position = starting_cell.to_isometric();

                let direction = target_isometric_position.sub(start_isometric_position);

                match direction.try_normalize() {
                    Some(normalized_direction) => {
                        let step = time.delta_seconds() * SPEED * normalized_direction;

                        if step.length() > direction.length() {
                            //transform.translation += direction.extend(0.);
                            hero.position += direction;
                        } else {
                            hero.position += step;
                            //transform.translation += step.extend(0.);
                        }
                    }
                    None => {}
                }
            }

            Movement::Stand { cell } => {
                hero.position = cell.to_isometric();
                //transform.translation = cell.to_isometric().add(UP_ALIGNMENT).extend(0.);
            }
        }
    }
}

pub fn update_translation(mut query: Query<(&Hero, &mut Transform)>) {
    for mut bundle in &mut query {
        let hero: &Hero = bundle.0;
        let transform: &mut Transform = bundle.1.as_mut();

        transform.translation = hero.position.add(UP_ALIGNMENT).extend(0.);
    }
}

pub fn update_face_direction(mut query: Query<(&mut Hero, &mut TextureAtlasSprite)>) {
    for (mut hero, mut sprite) in &mut query {
        match hero.movement.face_direction() {
            Some(new_face_direction) => {
                if new_face_direction != hero.direction {
                    hero.direction = new_face_direction;

                    sprite.index = hero.direction.index();
                }
            }
            None => {}
        }
    }
}

pub fn update_ordered_path(
    mut query: Query<&mut Hero>,
    mut mouse_button_input_events: EventReader<MouseButtonInput>,
    windows: Query<&Window>,
    camera_q: Query<(&Camera, &GlobalTransform)>,
    scenario: Query<&crate::scenario::Scenario>,
) {
    for event in mouse_button_input_events.iter() {
        if event.button != MouseButton::Right || event.state != ButtonState::Pressed {
            break;
        }

        // if game is in the talk stage, doesn't move and quit function
        let s: &crate::scenario::Scenario = scenario.single();

        if let Some(stage) = s.stages.front() {
            if let crate::scenario::Stage::TALK { sentence: _ } = stage {
                break;
            }
        }

        let (camera, camera_transformation) = camera_q.single();

        let window = windows.single();

        if let Some(ordered_target) = get_cell_coord(window, camera, camera_transformation) {

            for mut hero in &mut query {
                match &hero.movement {
                    Movement::Stand { cell } => {
                        let path = calculate_path(cell, &ordered_target);

                        hero.path = path;
                        //println!(" ordered path: {:?}", path);
                    }
                    Movement::Move {
                        starting_cell,
                        target_cell,
                    } => {
                        // check if starting_cell is nearer than target_cell
                        let distance_between_start_and_ordered_target: f32 =
                            starting_cell.sub(&ordered_target).length();
                        let distance_between_current_target_and_ordered_target: f32 =
                            target_cell.sub(&ordered_target).length();

                        if distance_between_start_and_ordered_target
                            < distance_between_current_target_and_ordered_target
                        {
                            let path = calculate_path(&starting_cell, &ordered_target);

                            let new_movement = Movement::Move {
                                starting_cell: target_cell.clone(),
                                target_cell: starting_cell.clone(),
                            };

                            hero.path = path;
                            hero.movement = new_movement;
                        } else {
                            let path = calculate_path(&target_cell, &ordered_target);

                            hero.path = path;
                        }
                    }
                }
            }
        }
    }
}

fn calculate_path(start: &Cell, target: &Cell) -> Vec<Cell> {
    let mut path: Vec<crate::map::Cell> = Vec::new();

    let x = target.x - start.x;
    let x_step = if x != 0 { x / x.abs() } else { 0 };

    //info!("x={}, x_step={}", x, x_step);

    let y = target.y - start.y;
    let y_step = if y != 0 { y / y.abs() } else { 0 };

    let mut x_mover = start.x;
    let mut y_mover = start.y;

    while x_mover != target.x && y_mover != target.y && x_step != 0 && y_step != 0 {
        x_mover += x_step;
        y_mover += y_step;

        path.push(Cell {
            x: x_mover,
            y: y_mover,
        });

        // info!("w1: x_mover={}, y_mover={}", x_mover, y_mover);
    }
    while x_mover != target.x && x_step != 0 {
        x_mover += x_step;

        path.push(Cell {
            x: x_mover,
            y: y_mover,
        });

        //info!("w2: x_mover={}, y_mover={}", x_mover, y_mover);
    }

    while y_mover != target.y && y_step != 0 {
        y_mover += y_step;

        path.push(Cell {
            x: x_mover,
            y: y_mover,
        });

        //info!("w3: x_mover={}, y_mover={}", x_mover, y_mover);
    }
    path
}

fn get_cell_coord(
    window: &Window,
    camera: &Camera,
    camera_transformation: &GlobalTransform,
) -> Option<crate::map::Cell> {
    if let Some(world_position) = window
        .cursor_position()
        .and_then(|cursor| camera.viewport_to_world(camera_transformation, cursor))
        .map(|ray| ray.origin.truncate())
    {
        let c = crate::map::Cell::from_isometric(world_position.x, world_position.y);

        Option::Some(c)
    } else {
        None
    }
}

fn picture_sheet(
    pos: Vec2,
    asset_server: &Res<AssetServer>,
    texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
) -> SpriteSheetBundle {
    let texture_handle = asset_server.load("mage_sheet_2.png");
    let texture_atlas =
        TextureAtlas::from_grid(texture_handle, Vec2::new(200.0, 200.0), 1, 8, None, None);

    let texture_atlas_handle = texture_atlases.add(texture_atlas);

    let mut sprite = TextureAtlasSprite::new(FaceDirection::RightFront.index());
    sprite.custom_size = Some(crate::constants::HERO.vec());

    SpriteSheetBundle {
        texture_atlas: texture_atlas_handle,
        sprite: sprite,
        transform: Transform::from_translation(pos.extend(6.)),
        ..default()
    }
}