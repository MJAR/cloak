use std::collections::VecDeque;
use std::ops::Add;

use bevy::input::mouse::MouseButton;
use bevy::input::mouse::MouseButtonInput;
use bevy::input::ButtonState;
use bevy::prelude::*;

use bevy::sprite::MaterialMesh2dBundle;

pub enum Stage {
    MOVE { book_position: crate::map::Cell, id: String },
    TALK { sentence: String },
}

#[derive(Component, Clone)]
pub struct Achievement {
    id: String,
    offset: Vec2,
    achieved: bool
}

#[derive(Component)]
pub struct AchievementText;

#[derive(Component)]
pub struct DialogText;

#[derive(Component)]
pub struct Scenario {
    pub stages: VecDeque<Stage>,
    pub achievements: Vec<String>
}

pub fn setup(mut commands: Commands) {
    let stages: VecDeque<Stage> = VecDeque::from([
        Stage::TALK {
            sentence: String::from("What is that void space around me? Where I am?"),
        },
        Stage::TALK {
            sentence: String::from("And ... more important question ... who I am?"),
        },
        Stage::TALK {
            sentence: String::from("That void inside me..."),
        },
        Stage::TALK {
            sentence: String::from("...is the same as that outside"),
        },
        Stage::TALK {
            sentence: String::from("Look! there is a book not far away from me"),
        },
        Stage::TALK {
            sentence: String::from("Such whiteness of pages..."),
        },
        Stage::TALK {
            sentence: String::from("something inside me desires it"),
        },
        Stage::TALK {
            sentence: String::from("like empty space which wants to be filled... "),
        },
        Stage::TALK {
            sentence: String::from("... with a matching shape"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell { x: 3, y: 3 },
            id: String::from("PWR")
        },
        Stage::TALK {
            sentence: String::from("something appears in my memory"),
        },
        Stage::TALK {
            sentence: String::from("high buildings, lecture rooms, people"),
        },
        Stage::TALK {
            sentence: String::from("corridors full of outdated magic"),
        },
        Stage::TALK {
            sentence: String::from("the black spells of processor architectures"),
        },
        Stage::TALK {
            sentence: String::from("entangled tricks of digital systems"),
        },
        Stage::TALK {
            sentence: String::from("deadly programming rituals..."),
        },
        Stage::TALK {
            sentence: String::from("my Alma Mater - Wroclaw University of Technology"),
        }
        ,
        Stage::TALK {
            sentence: String::from("...Faculty of Electronics... "),
        },
        Stage::TALK {
            sentence: String::from("... Computer Science ..."),
        },
        Stage::TALK {
            sentence: String::from("Look... there is another book"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell { x: 0, y: 0 },
            id: String::from("SPARK")
        },
        Stage::TALK {
            sentence: String::from("Yeah... I remember now..."),
        },
        Stage::TALK {
            sentence: String::from("sweet taste of summoning magic..."),
        },
        Stage::TALK {
            sentence: String::from("swarm of processes spawned across computer cluster..."),
        },
        Stage::TALK {
            sentence: String::from("graph algorithm implemented in functional language..."),
        },
        Stage::TALK {
            sentence: String::from("Dense MST computed by heavy usage of RAM..."),
        },
        Stage::TALK {
            sentence: String::from("... Spark Apache ..."),
        },
        Stage::TALK {
            sentence: String::from("That word of power thunders in my soul..."),
        },
        Stage::TALK {
            sentence: String::from("... and brings memory of my engineer thesis..."),
        },
        Stage::TALK {
            sentence: String::from("... rated by mage supervisor as 5.5"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell { x: -3, y: 0 },
            id: String::from("SCALA")
        },
        Stage::TALK {
            sentence: String::from("But all of that will not be possible ..."),
        },
        Stage::TALK {
            sentence: String::from("... without Martin Odersky ..."),
        },
        Stage::TALK {
            sentence: String::from("who show me the path to the world of functional languages"),
        },
        Stage::TALK {
            sentence: String::from("thanks to his spell language ..."),
        },
        Stage::TALK {
            sentence: String::from("... SCALA ..."),
        }
        ,
        Stage::TALK {
            sentence: String::from("... and his magical course on Coursera."),
        }
        ,
        Stage::TALK {
            sentence: String::from("It is true that Scala is overcomplicated"),
        },
        Stage::TALK {
            sentence: String::from("and because of that is unpractical except some specific cases"),
        }
        ,
        Stage::TALK {
            sentence: String::from("But thanks to Martin Odersky I became familiar with many concepts..."),
        }
        ,
        Stage::TALK {
            sentence: String::from("... which later has been introduced into other spellbooks like Java..."),
        },
        Stage::TALK {
            sentence: String::from("...and finally founds its most practical implementation in Rust."),
        },
        Stage::MOVE {
            book_position: crate::map::Cell{x: -6, y: -3},
            id: String::from("C++") 
        },
        Stage::TALK {
            sentence: String::from("After graduating, I took a month-long Nokia Academy course..."),
        },
        Stage::TALK {
            sentence: String::from("... which let me enhance my C++ knownledge ..."),
        },
        Stage::TALK {
            sentence: String::from("... and taste secrets of Template Programming."),
        },
        Stage::TALK {
            sentence: String::from("Other things like TDD, basic software patterns, good practices of clean code... "),
        },
        Stage::TALK {
            sentence: String::from("... was also tought during this course."),
        },
        Stage::MOVE {
            book_position: crate::map::Cell{x: -5, y: -2},
            id: String::from("NOKIA") 
        },
        Stage::TALK {
            sentence: String::from("Then I finally got my first job in the Nokia ..."),
        },
        Stage::TALK {
            sentence: String::from("... in the Radio Frequency Software Department ... "),
        },
        Stage::TALK {
            sentence: String::from("... where I had to face all challenges ..."),
        },
        Stage::TALK {
            sentence: String::from("... imposed by complexity of software which drives modern radio modules"),
        },
        Stage::TALK {
            sentence: String::from("What a dragons are hidden in the code !!!!"),
        },
        Stage::TALK {
            sentence: String::from("What a swarm of mischievous imps haunts in the hardware !!!"),
        },
        Stage::TALK {
            sentence: String::from("Fortunatelly from all that battles I emerged victorious"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell{x: 0, y: 0},
            id: String::from("TL") 
        },
        Stage::TALK {
            sentence: String::from("After few years I became the Technical Leader ..."),
        },
        Stage::TALK {
            sentence: String::from("... and together with my team I gave battle ..."),
        },
        Stage::TALK {
            sentence: String::from("... to all new requirements and problems which emerged ..."),
        },
        Stage::TALK {
            sentence: String::from("... to threaten and destroy our backlog"),
        },
        Stage::TALK {
            sentence: String::from("There is a list of my mighty battle spells ..."),
        },
        Stage::TALK {
            sentence: String::from("... requirement analyses and negotiations ..."),
        },
        Stage::TALK {
            sentence: String::from("... software UML design ..."),
        },
        Stage::TALK {
            sentence: String::from("... splitting task between team members ..."),
        },
        Stage::TALK {
            sentence: String::from("... estimating time of development and integration ..."),
        },
        Stage::TALK {
            sentence: String::from("... projecting the scope of the tests which have to be done on real radiomodule ..."),
        },
        Stage::TALK {
            sentence: String::from("... and constant help to every team member who needed technical assistance."),
        },
        Stage::TALK {
            sentence: String::from("Yeah... the sight of victorious flag raised proudly on the battlefield..."),
        },
        Stage::TALK {
            sentence: String::from("... still brings light to my soul"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell{x: 3, y: 4},
            id: String::from("ARCH") 
        },
        Stage::TALK {
            sentence: String::from("At some moment I was delegated as a support for Architecture Team ..."),
        },
        Stage::TALK {
            sentence: String::from("... where I was given a task to supervise the birth of the new Nokia product ..."),
        },
        Stage::TALK {
            sentence: String::from("... and to detects in advance emerging architecture problems"),
        },
        Stage::TALK {
            sentence: String::from("So i opened my THIRD INNER EYE ... "),
        },
        Stage::TALK {
            sentence: String::from( "...I found the problem which was coming to us..."),
        },
        Stage::TALK {
            sentence: String::from("...and together with the rest of Arch team we developed solution..."),
        },
        Stage::TALK {
            sentence: String::from("...which made possible use of the same code..."),
        },
        Stage::TALK {
            sentence: String::from("... for the two projects with the same hardware ..."),
        },
        Stage::TALK {
            sentence: String::from("... but with entirely different set of requirements"),
        },
        Stage::MOVE {
            book_position: crate::map::Cell{x: 6, y: 4},
            id: String::from("RUST") 
        },
        Stage::TALK {
            sentence: String::from("Experience with practical usage of magic ..."),
        },
        Stage::TALK {
            sentence: String::from("... shouldn't stop us from learning new things"),
        },
        Stage::TALK {
            sentence: String::from("So I started to study the mysteries of RUST..."),
        },
        Stage::TALK {
            sentence: String::from("... and its noble rules ..."),
        },
        Stage::TALK {
            sentence: String::from("... NO EXCEPTIONS ..."),
        },
        Stage::TALK {
            sentence: String::from("... NO NULLS ..."),
        },
        Stage::TALK {
            sentence: String::from("... NO MANUAL DEALLOCATION ..."),
        },
        Stage::TALK {
            sentence: String::from("... NO INHERITANCE ..."),
        },
        Stage::TALK {
            sentence: String::from("and started to follow its paths to wisdom..."),
        },
        Stage::TALK {
            sentence: String::from("... PRIMARY OF MOVE SEMANTICS ..."),
        },
        Stage::TALK {
            sentence: String::from("... BORROW CHECKER ..."),
        },
        Stage::TALK {
            sentence: String::from("... TYPE PATTERN MATCHING ..."),
        },
        Stage::TALK {
            sentence: String::from("... CARGO ..."),
        },
        Stage::TALK {
            sentence: String::from("stop"),
        },
        Stage::TALK {
            sentence: String::from("Too many jewels of wisdom shine in the crown of RUST..."),
        },
        Stage::TALK {
            sentence: String::from("... to list all of them"),
        },
        Stage::TALK {
            sentence: String::from("It is enough to say that thanks to RUST I exist"),
        }
        ,
        Stage::TALK {
            sentence: String::from("The little impersonalization of Programmer..."),
        },
        Stage::TALK {
            sentence: String::from("... who threw me into this space of oblivion..."),
        },
        Stage::TALK {
            sentence: String::from("... and called me to carry the burden of my miserable existence ..."),
        },
        Stage::TALK {
            sentence: String::from("... to the enjoyment of all of his potential employeers"),
        },
        Stage::TALK {
            sentence: String::from("I hate him"),
        }
    ]);

    let achievements: Vec<String> = Vec::new(); 
    commands.spawn(Scenario { stages, achievements });

    let text_bundle = Text2dBundle {
        text: Text::from_section(
            "",
            TextStyle {
                font_size: 14.0,
                color: Color::BLACK,
                ..default()
            },
        )
        .with_alignment(TextAlignment::Center),
        transform: Transform::from_translation(Vec3 {
            x: 0.,
            y: 0.,
            z: 1.,
        }),
        ..default()
    };

    commands.spawn((DialogText{}, text_bundle));
}

fn text_bundle(text: &str, position: &Vec2) -> Text2dBundle {
    Text2dBundle {
        text: Text::from_section(
            String::from(text),
            TextStyle {
                font_size: 10.0,
                color: Color::WHITE,
                ..default()
            },
        )
        .with_alignment(TextAlignment::Center),
        transform: Transform::from_translation(position.extend(3.)),
        ..default()
    }
}

pub fn place(radius: f32, achievements: & mut Vec<Achievement>){

    let margin: f32 = 5.;
    let y_offset: f32 = -100.;

    let l = achievements.len();

    let start_point: f32 = if l % 2 == 0 {
         - ( (l/2 - 1) as f32 ) * (2. * radius + margin) - radius - margin/2.
    }
    else {
         - ( (l / 2) as f32 ) * (2. * radius + margin)
    };

    let step: f32 = radius * 2. + margin;

    for (i, a) in achievements.iter_mut().enumerate() {
        a.offset.y = y_offset;
        a.offset.x = start_point + (i as f32) * step;
    }
}

pub fn setup_achievements(mut commands: Commands, mut meshes: ResMut<Assets<Mesh>>, mut materials: ResMut<Assets<ColorMaterial>>){

    let mut achievements = vec![
        Achievement{
            id: String::from("PWR"),
            offset: Vec2 { x: -125., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("SPARK"),
            offset: Vec2 { x: -55., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("SCALA"),
            offset: Vec2 { x: -55., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("C++"),
            offset: Vec2 { x: -90., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("NOKIA"),
            offset: Vec2 { x: -20., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("TL"),
            offset: Vec2 { x: -20., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("ARCH"),
            offset: Vec2 { x: -20., y: -80. },
            achieved: false
        },
        Achievement{
            id: String::from("RUST"),
            offset: Vec2 { x: -20., y: -80. },
            achieved: false
        }
    ];

    place(15., & mut achievements);

    for a in achievements {

        commands.spawn((AchievementText{}, a.clone(), text_bundle(&a.id, &a.offset)));

        commands.spawn((MaterialMesh2dBundle {
            mesh: meshes.add(shape::Circle::new(15.0).into()).into(),
            material: materials.add(ColorMaterial::from(Color::BLUE)),
            transform: Transform::from_translation(a.offset.extend(2.0)),
            ..default()
        }, a));
    }
}

pub fn update_achievment_position_for_camera(mut achievement_query: Query<(& Achievement, & mut Transform)>, hero_query: Query<&crate::hero::Hero>) {
    for h in & hero_query {
        for (achievement, mut transform) in & mut achievement_query {
            transform.translation = achievement.offset.add(h.position).extend(2.);
        }
    }
}

pub fn update_text_position(hero_query: Query<&crate::hero::Hero>, mut text_query: Query<& mut Transform, With<DialogText>> ) {
    for mut transform in & mut text_query {
        for hero in &hero_query {
            transform.translation = hero.position.add(Vec2{x:0., y: 70.}).extend(1.);
        }
    }
}

pub fn update_after_mouse_click(
    mut mouse_button_input_events: EventReader<MouseButtonInput>,
    mut scenario: Query<&mut Scenario>
) {
    for event in mouse_button_input_events.iter() {
        if event.button != MouseButton::Right || event.state != ButtonState::Pressed {
            break;
        }
        if let Some(stage) = scenario.single_mut().stages.front() {
            if let Stage::TALK { sentence: _ } = stage {
                scenario.single_mut().stages.pop_front();
            }
        }
    }
}

pub fn update_text_according_to_scenario_stage(scenario: Query<&Scenario>, mut text: Query<&mut Text, With<DialogText>>) {

    for s in &scenario {
        for mut t in & mut text {

            if let Some(stage) = s.stages.front() {
                if let Stage::TALK{sentence} = stage {
                    t.sections[0].value = sentence.clone();
                }
                else {
                    t.sections[0].value = String::from("");
                }
            }
            else {
                t.sections[0].value = String::from("");
            }
        }
    }
}

pub fn update_for_hero_move(
    mut scenario_query: Query<&mut Scenario>,
    mut hero_query: Query<&mut crate::hero::Hero>,
    mut achievements: Query<&mut Achievement> 
) {
    let mut hero = hero_query.single_mut();
    let stages: &mut VecDeque<Stage> = &mut scenario_query.single_mut().stages;

    if let Some(stage) = stages.front() {
        if let Stage::MOVE { book_position, id } = stage {
            if &hero.movement.current_position() == book_position {
                hero.movement = crate::hero::Movement::Stand {
                    cell: book_position.clone(),
                };

                for mut a in & mut achievements {

                    if a.id.eq(id) {
                        a.achieved = true;
                    }
                    
                }

                hero.path.clear();
                stages.pop_front();
            }
        }
    }
}

pub fn update_achievement_graphics_according_to_visibility(mut graphics: Query<(& mut Visibility, &Achievement)> ) {
    for (mut v, a) in & mut graphics {
        *v = if a.achieved { Visibility::Visible } else { Visibility::Hidden };
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Component)]
pub struct Book {
    cell: crate::map::Cell,
    title: String,
}

pub fn book_update(mut commands: Commands, asset_server: Res<AssetServer>, book_query: Query<Entity, With<Book>>, scenario_query: Query<&Scenario>) {
    
    for b in &book_query {
        for scenario in &scenario_query {
            if let Some(stage) = scenario.stages.front() {
                if let Stage::TALK{sentence: _} = stage {
                    commands.entity(b).remove::<SpriteBundle>();
                    commands.entity(b).remove::<Book>();
                }
            }
        }
    }

    if book_query.is_empty() {
        for scenario in &scenario_query {
            if let Some(stage) = scenario.stages.front() {
                if let Stage::MOVE{book_position, id} = stage {


                    //info!("adding book for MOVE: {}", id);
                    let book = Book {
                        cell: book_position.clone(),
                        title: id.clone()
                    };
                
                    let sprite = Sprite {
                        custom_size: Some(Vec2 { x: 30., y: 16. }),
                        ..default()
                    };
                
                    let sprite_bundle = SpriteBundle {
                        texture: asset_server.load("book.png"),
                        transform: Transform::from_translation(book.cell.to_isometric().extend(-1.)),
                        sprite: sprite,
                        ..default()
                    };

                    commands.spawn((book, sprite_bundle));
                }
            }
        }
    }
}